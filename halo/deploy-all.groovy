def ansible = docker.image('pi4k8s/ansible:centos7')
node {

    stage('git chekout') {
        parallel(
                'common': {
                    dir('common'){
                        git branch: 'master', url: 'https://gitee.com/pi4k8s/ansible-exmaples.git '
                    }
                },
                'halo': {
                    dir('halo'){
                        git branch: 'master', url: 'https://gitee.com/cuiyingfeng/jenkins-ansible-examples.git'
                    }

                }
        )
    }
    stage('aggregate') {
        sh 'rm -rf aggregate && mkdir aggregate'
        sh 'cp -r ./halo/halo ./aggregate'
        sh 'cp -r ./common/aliyun-centos7.9/playbook/roles/python3 ./aggregate/halo/playbook/roles'
        sh 'cp -r ./common/aliyun-centos7.9/playbook/roles/docker ./aggregate/halo/playbook/roles'

    }
    stage('install-all') {
        ansible.inside("") {
            sh "cd aggregate/halo && ANSIBLE_HOST_KEY_CHECKING=false " +
                    "ansible-playbook playbook/install-all-from-scratch.yaml -i hosts -e env_hosts=halohost -e SERVER_NAME=www.cuiyingfeng.com"
        }
    }
}
